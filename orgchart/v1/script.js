var vertMargin = 15;
var horizMargin = 25;
var shiftMargin	= 0.075;

var boxWidth = 180;
var boxHeight = 40;

var colWidth = boxWidth + horizMargin;
var rowHeight = boxHeight + vertMargin;

var topConnection = boxHeight*(1/3);
var bottomConnection = boxHeight*(2/3);
var middleConnection = boxHeight*(1/2);
var vWideTunnel = horizMargin*(3/4);
var vTightTunnel = horizMargin*(1/4);
var vMidTunnel = horizMargin*(1/2);

var breakpoint = 600;
//grab tree data json file

var myChart;

d3.request('tree.json')
  .mimeType("application/json")
  .get(function(err,data){
    var json = JSON.parse(data.response);
    var nodes = [];
    //convert each tree object into an Entry object
    var entry = {};
    _.each(json, function(e,i,c) {
      entry = new Entry(e, c);
      nodes.push(entry);
    });
    //create chart from list of Entries;
    myChart = new OrgChart(nodes);
    kickstart(myChart);
  });

$(window).resize(function(){
  kickstart(myChart);
});

function kickstart (chart){
  var windowWidth = $(window).width();
  if (windowWidth > breakpoint){
    drawFull(chart);
  }
  else{
    var topLevel = _.chain(chart.nodes).filter(function(o){return o.root || o.depth == 0}).uniq().orderBy(['root', 'depth', 'children', 'group'], ['desc', 'asc', 'desc','asc']).value();
    drawMobile(topLevel, chart.nodes);
  }
};

function drawFull(chart) {
    d3.selectAll('svg').remove();
    d3.select('.output')
      .append('svg')
      .attr("width", (chart.cols + 1) * colWidth)
      .attr("height", (chart.rows+ 1) * rowHeight);
    var svg = d3.select('svg');
    var links = makeLinks(chart.nodes);
    svg.selectAll('.link')
      .data(links)
      .enter().append('path')
      .attr('class', 'link')
      .attr('d', drawLink)
      .attr('id', function (d) {return d.id})
    svg.selectAll('.box')
      .data(chart.nodes)
      .enter().append('rect')
      .attr('class', 'box')
      .attr('width', function(d) {return boxWidth * (1 - (d.depth * shiftMargin))})
      .attr('height', boxHeight)
      .attr('id', function (d) { return d.id + "Box"; })
      .attr('y', function (d) {return d.row * rowHeight})
      .attr('x', function(d){ return d.col * colWidth})
    svg.selectAll('.label')
      .data(chart.nodes)
      .enter().append('text')
      .attr('class', 'label')
      .attr('id', function (d) { return d.id; })
      .html(linkName)
      .attr('y', function (d) {return (d.row * rowHeight) + 20; })
      .attr('x', centerText)
};

function drawMobile (selection, full) {
  d3.selectAll('svg').remove();
  d3.select('.output')
    .append('svg')
    .attr("width", colWidth)
    .attr("height", (selection.length) * rowHeight);
  var svg = d3.select('svg');
  svg.selectAll('.box')
    .data(selection)
    .enter().append('rect')
    .attr('class', 'box')
    .attr('width', function(d) {return d.root ? boxWidth : boxWidth * (1 - (d.depth+1) * shiftMargin)})
    .attr('height', boxHeight)
    .attr('id', function (d) { return d.id + "Box"; })
    .attr('y', mobileRow)
    .attr('x', 0)
    .attr('depth', function(d) {return d.depth})
//    .attr('visibility', hideAtMobile)
    .on('click', function(d,i,c) {
      if (d.children){
        var newSel = _.chain(full).filter(function(o){ return (o.root || o.id == d.id || _.find(o.parents, function (n){return n.id === d.id})) }).uniq().orderBy(['root', 'depth', 'children', 'group'], ['desc', 'asc', 'desc','asc']).value();
        drawMobile(newSel, full); 
        showHide(d,i,c);
      }
    });
  svg.selectAll('.label')
    .data(selection)
    .enter().append('text')
    .attr('class', 'label')
    .attr('id', function (d) { return d.id; })
    .html(linkName)
    .attr('y', mobileRow)
    .attr('x', centerText)
    .attr('depth', function(d) {return d.depth})
//    .attr('visibility', hideAtMobile);
  svg.selectAll('.link')
    .data(selection)
    .enter().append('path')
    .attr('class', 'link')
    .attr('d', drawMobileLink)
    .attr('id', function (d) {return d.id})
    .attr('depth', function(d) {return d.depth})
//    .attr('visibility', hideAtMobile)
};

var showHide = function (d,i,c) {
  var el = $(c[i]);
  if (el.attr('visibility') == 'visible') {
    var cornerX = (parseInt(el.attr('x')) + parseInt(el.attr('width')) - 50)
    var cornerY = (parseInt(el.attr('y')) - 15);
    if(!$('#close-button').length) {
      d3.select('svg')
        .append('text')
        .attr('class', 'label')
        .attr('id', 'close-button')
        .attr('y', cornerY)
        .attr('x', cornerX)
        .text('close')
        .on('click', function(){
          $('rect[depth="0"], text[depth=0], path[depth="0"]').attr('visibility', 'visible');
          $('rect[depth!="0"], text[depth!=0], path[depth!="0"]').attr('visibility', 'hidden');
        this.remove();
      });
    }
    var children = _.filter(c, function(o) {return o.__data__ !== d && (o.__data__.root || o.__data__.group == d.group || _.find(o.__data__.parents, {"id": d.id}))});
    if (children.length > 0) {
      _.each(children, function(e,i,c){
        e = e.__data__
        $('#'+e.id + ', #'+e.id+'Box, #'+d.id+'-'+e.id).attr('visibility', 'visible');
        if (e.children > 0) {
          showHide(e,i,c);
        }
      });
    }
    var others = _.filter(c, function(o) {return o.__data__ !== d && !o.__data__.root && !(o.__data__.group == d.group || _.find(o.__data__.parents, {"id": d.id}))}); 
    if (others.length > 0){
      _.each(others, function(e,i,c){
        e = e.__data__;
        $('#'+e.id + ', #'+e.id+'Box, [id^="'+e.id+'"], [id$="-'+e.id+'"]').attr('visibility', 'collapse');
      });
    }

  }
}

var hideAtMobile = function (d,i,c) {
  if (windowWidth < 600 && d.depth > 0) {
    return 'collapse';
  }
  else return 'visible';
};

var centerText = function (d, i, c) {
	var box = $('#' + d.id + 'Box');
  var textWidth = this.clientWidth;
  var width = parseInt(box.attr('width'));
  var x = parseInt(box.attr('x')) + (width-textWidth)/2;
	return x;
};

var mobileText = function (d,i,c) {}

var linkName = function (d,i,c) {
  if (d.href.length > 0) {
    return '<a href="' + d.href + '">' + d.name + '</a>';
  }
  else {
    return d.name;
  }
};

var mobileRow = function (d,i,c){
  var thisIndex = _.findIndex(c, function(o){return _.startsWith($(o).attr('id'), d.id)});
  if (thisIndex !== undefined){
    if (this.tagName == 'text'){
      return thisIndex * rowHeight + 20;
    }
    else {
      return thisIndex * rowHeight;
    }
  }
  else return 0;
}

var drawLink = function (d,i,c) {
	var link = '';
	//move to starting position
	link += 'M' + d.source.x + ',' + d.source.y;
	//add any elbows necessary
	_.each(d.elbows, function(e) {
    if (e.x) {
      link += ' H' + e.x;
    }
    else {
      link += ' V' + e.y;
    }
  });
	//connect to target
  link += ' V' + d.target.y +  ' H' + d.target.x;
	return link;
};

var drawMobileLink = function(d,i,c){
  if (d.parents.length > 0) {
    var target = $('#' + d.id + 'Box');
    for(var i = 0; i < d.parents.length; i++) {
      var source = $('#' + d.parents[i].id + 'Box');
      if (!source.length){
        continue;
      }
      else {
        link = 'M' + (parseInt(source.attr('x')) + parseInt(source.attr('width'))) 
          + ',' + (parseInt(source.attr('y')) + bottomConnection)
          + ' H' + (parseInt(source.attr('x')) + parseInt(source.attr('width')) + vMidTunnel)
          + ' V' + (parseInt(target.attr('y')) + topConnection)
          + ' H' + (parseInt(target.attr('x')) + parseInt(target.attr('width')));
        return link;
      }
    }
  }
}

function makeLinks(collection) {
  var result = [];
  _.each(collection, function (target, i) {
    if (target.parents) {
      _.each(target.parents, function (f, j) {
        var source = _.find(collection, {"id": f.id});
        var coordinates = {            
          "source": {},
          "target": {},
          "elbows": [],
          "depth": _.max([source.depth, target.depth]),
          "id": source.id + '-' + target.id
        };
        var blockCorner = null;
        var deepest = null;
        //handle all non-root routes
        if (!source.root) {
          //start with coordinates in top left corner
          coordinates.source = {
              "x": (source.col * colWidth) + (boxWidth * (1 - (source.depth * shiftMargin))),
              "y": source.row * rowHeight
            };
          coordinates.target = {
              "x": (target.col * colWidth) + (boxWidth * (1 - (target.depth * shiftMargin))),
              "y": target.row * rowHeight,
            };
          //move starting point to right side of box
          //handle lines moving to the right
          if (target.col > source.col) {
            //leave and enter from the top half of the box
            coordinates.source.y += topConnection;
            coordinates.target.y += topConnection;
            //add a temp point halfway through the right gutter
            coordinates.elbows.push({"x": coordinates.source.x + vMidTunnel});
            //handle links spanning multiple columns
            if (target.col - source.col > 1) {
              deepest = null;
              for (var i = source.col + 1; i <= target.col; i++) {
                //find the lowest point of all columns along the way
                blockCorner = _.chain(collection).filter(function(o) {return o.col == i}).maxBy(function (o){return o.row;}).value();
                //add waypoint below the lowest point in the in between column
                if (deepest === null || blockCorner.row > deepest) {
                  deepest = blockCorner.row;
                }
                //push line out to the next column
              }
              coordinates.elbows.push({"y": (deepest) * rowHeight + topConnection});
              coordinates.elbows.push({"x": (target.col) * colWidth - vMidTunnel});
            }
          }
          //handle links moving right to left
          else if (target.col < source.col){
            //move exit point to right side of the box
            coordinates.source.x -= (boxWidth * (1 - (source.depth * shiftMargin)));
            coordinates.source.y += topConnection;
            coordinates.target.y += topConnection;
            //add a midpoint to the left
            coordinates.elbows.push({"x": ((source.col - 1) * colWidth) + boxWidth + vMidTunnel});
            if (source.col - target.col > 1) {
              deepest = null;
              for (var i = source.col - 1; i >= target.col; i--) {
                //find the lowest point of all columns along the way
                blockCorner = _.chain(collection).filter(function(o) {return o.col == i}).maxBy(function (o){return o.row;}).value();
                //add waypoint below the lowest point in the in between column
                if (deepest === null || blockCorner.row > deepest) {
                  deepest = blockCorner.row;
                }
                //push line out to the next column
              }
              coordinates.elbows.push({"y": deepest * rowHeight + topConnection});
              coordinates.elbows.push({"x": target.col * colWidth + boxWidth + vMidTunnel});

            }
          }
          //handle children columns
          else {
            //exit from teh bottom connection and enter from the top
            coordinates.source.y += bottomConnection;
            coordinates.target.y += topConnection;
            //add a midpoint 1/4 of the way into the gutter
            coordinates.elbows.push({"x": coordinates.source.x + vTightTunnel});
          }
        }
        //handle links from the root
        else {
          coordinates.source = {
              "x": source.col * colWidth,
              "y": source.row * rowHeight + bottomConnection
            };
          coordinates.target = {
              "x": target.col * colWidth,
              "y": target.row * rowHeight
            };
          //for nodes in the next column outside the root group
          if (target.row == source.row + 1 && target.group != source.group) {
            if (target.col > source.col){
              //start from the right side of the box
              coordinates.source.x += boxWidth;
            }
            //add a waypoint above the middle of the target
            coordinates.elbows.push({"x": target.col * colWidth + boxWidth / 2})
          }
          //children in lower columns, or members of the same group
          else {
            //enter from middle connection;
            coordinates.target.y += middleConnection;
            if (target.col == source.col) {
              coordinates.elbows.push({"x": target.col * colWidth + boxWidth + vTightTunnel})
            }
            else {
              coordinates.elbows.push({"x": target.col * colWidth + boxWidth + vMidTunnel})
            }
            coordinates.elbows.push({"y": coordinates.target.y});
          }
        }
        result.push(coordinates);
    });
  }
});
return result;
}