/*
 Object for each entry in the org chart. Accepts an object of similar structure, and uses "parents" value to preset a
 row and depth for the root node of the chart'
 */

function Entry (obj) {
  this.id = obj.id;
  this.name = obj.name;
  this.href = obj.href;
  this.group = obj.group;
  this.title = obj.title;
  this.parents = obj.parents || [];
  this.children = 0;
  this.row = this.parents.length === 0 ? 0 : null;
  this.depth = this.parents.length === 0 ? 0 : null;
  this.col = null;
  this.side = '';
  this.root = this.parents.length === 0;
}

/*
    Object for the full org chart. Includes several function for parsing through an array of nodes;
 */

function OrgChart (nodes) {
  /*
      initialization;
   */
  this.nodes = process(nodes);
  this.rows = _.maxBy(this.nodes, function (o) {return o.row}).row;
  this.cols = _.maxBy(this.nodes, function (o) {return o.col}).col;

  /*
  Runs the main functions below;
   */
  function process(collection) {
    var result = collection.slice();
    result = calculateChildren(result);
    result = findRows(result);
    result = findDepths(result);
    result = calculateCols(result);
    result = _.sortBy(result, ['row', 'col']);
    return result;
  }

  /*
   * Helper function: loops through parents, executing a callback;
   */
  function forParents(child, collection, callback) {
    if (child.parents.length > 0) {
      _.each(child.parents, function(e) {
        var parent = _.find(collection, {"id": e.id});
        if (parent) {
          callback(child, parent, collection);
        }
      });
    }
  }

  function findRows(collection) {
    var result = [];
    groups = _.chain(collection).orderBy('children', 'desc').groupBy('group').value();
    _.each(groups, function (e) {
      e = _.groupBy(e, 'depth');
      _.each(e, function(f){
        _.each(f, function(g,h) {
          if (g.root){
            g.row = 0;
          }
          else if (g.row === null || h+1 > g.row) {
            g.row = h+1;
          }
          result.push(g);
        });
      });
    });
    return result;
  }


  function findDepths(collection) {
    var result = collection.slice();
    result = _.orderBy(result, 'children', 'desc');
    _.each(result, function (e) {
      if (!e.root) {
        forParents(e, result, function (child, parent, collection) {
          if (parent.root) {
            e.depth = 0;
          }
          else if (e.depth == null || parent.depth + 1 > e.depth)  {
              e.depth = parent.depth + 1;
          }
        });
      };
    });
    return result;
  }

  /*
      Determine how many chidren each node has.
   */
  function calculateChildren(collection) {
    var result = collection.slice();
    _.each(result, function (e, i) {
      forParents(e, result, function(child, parent) {
        parent.children++;
      });
    });
    result = _.orderBy(result, 'children', 'desc');
    return result;
  }

  /*
      Find the correct column for each node in the chart;
   */
  function calculateCols(collection) {
    //duplicate
    var input = collection.slice();
    var result = [];
    //sort nodes into separate groups for root rows, top row, and multi-parents
    var rootRow = _.filter(input, function(o) {return o.root});
    var topRow = _.chain(input).filter(function(o) {return o.row !== 0 && (o.depth === 0 || o.children > 0)}).orderBy(['row','parents'], ['asc', 'asc']).value();
    var moreParents = _.filter(input, function(o) {return o.parents.length > 1});
    //handle roots first
     _.each(rootRow, function(e,i){
      var immediateChildren = _.filter(topRow, function(o){ return (o.row == 1 && _.find(o.parents, {id:e.id}))});
      e.col = _.ceil(immediateChildren.length/2);
      result.push(e);
    });
    //place all single-parent nodes;
    _.each(topRow, function(e, i) {
      if (e.parents.length === 1){
        forParents(e, input, function(child, parent, colllection) {
          //for children of the root, outside the group, place them in the first open column
          if (parent.root){
            highest = _.maxBy(topRow, function(o){return o.col});
            if (!highest){
              e.col = 0;
            }
            //children in the same group go underneath the parent
            else if (e.group === parent.group) {
              e.col = parent.col;
            }
            // don't place non-group children under the root
            else {
              e.col = highest.col + 1 === parent.col ? highest.col + 2 : highest.col + 1;
            }
          }
          else {
            e.col = parent.col;
          }
          result.push(e);
        });
      }
      //cycle through children of each top-row node and place them under the parent
      if (e.children > 0){
        var children = _.filter(input, function(o) { return _.find(o.parents, {"id": e.id})});
        _.each(children, function(f, j){
          f.col = e.col;
          result.push(f)
        });
      }
    });
    //remove duplicates
    var result = _.uniq(result);
    //sort columns into a visually appealing arrangements
    var columns = _.groupBy(result, 'col');
    columns = wSort(columns, 'length');
    _.each(columns, function(e,i){
      _.each(e, function(f){
        var g = _.chain(result).find(function(o) {return o.id === f.id}).value();
        g.col = i;
      });
    });
    _.each(moreParents, function(e,i){
        var parentCols = [];
        //get each parents' column
        var lowest = -1;
        forParents(e, result, function(child, parent, collection) {
          if (parent.row > lowest){
            lowest = parent.row
            parentCols = [parent.col];
          }
          else if (parent.row == lowest) {
            parentCols.push(parent.col);
          }
        });
        //find the center between the parents
        var center = _.ceil(_.mean(parentCols));
        //get the lowest non-group point
        var overhead = _.chain(result).filter(function(o){return o.col === center && o.group !== e.group}).maxBy(function (o) {return o.row}).value();
        if (overhead){
          e.row = overhead.row + 2 + i;
          e.col = center;
          result.push(e);
        }
    });
    return result;
 }


}