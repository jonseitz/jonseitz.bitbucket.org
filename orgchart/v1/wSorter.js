
/**
 * Logical sorting function that places the largest value first, second largest last, and third largest in the middle
 * e.g. [5, 1, 3, 2, 4]
 **/
function wSort (input, sortValue) {
  //initialize results array
  var collection;
  if (input instanceof Array) {
    collection = input.slice();
  }
  else if (input instanceof Object){
    collection = _.cloneDeep(input);
  }
  if (!collection) {
    return null;
  }
  var sorted = _.sortBy(collection, function (o) {
    return -o
  });
  if (sortValue !== undefined) {
    sorted = _.sortBy(collection, function (o) {
      return -(o[sortValue]);
    });
  }
  if (collection.length < 3) {
    return sorted;
  } 
  var result = [];
  var firstVal;
  var lastVal;
  var middle;
  var middleVal;
  var even;
  _.each(sorted, function (e, i) {
    //get the value to be sorted
    var test = e;
    firstVal = result[0];
    lastVal = result[result.length - 1];
    middle = _.ceil(result.length / 2);
    middleVal = result[middle];
    even = result.length % 2 === 0;
    if (sortValue !== undefined) {
      test = e[sortValue];
      firstVal = firstVal ? firstVal[sortValue] : firstVal;
      middleVal = middleVal ? middleVal[sortValue] : middleVal;
      lastVal = lastVal ? lastVal[sortValue] : lastVal;
    }
    //add the first two elements to start
    if (result.length < 2) {
      result.push(e);
    }
    //if only two values, make test the middle
    else if (result.length === 2) {
      result.splice(middle, 0, e);
    }
    //check if even
    else if (even) {
      //if it's larger than the current middle value, raeplace that one
      if (test > middleVal) {
        result.splice(middle, 0, e);
      }
      //if not, place it before the right value
      else {
        result.splice(result.length - 1, 0, e);
      }
    }
    //different logic if odd
    else {
      //if larger than the middle place to the right of the middle value, making it the new middle value
      if (test > middleVal) {
        result.splice(middle + 1, 0, e);
      }
      //if smaller, keep the middle value in the correct place for the next pass by placing it after the left value
      else {
        result.splice(1, 0, e);
      }
    }
  });
  return result;
}